// each package's paragraph contains a number of information sections
// we care about three: Package, Depends and Description

// Package is simply the name of the package
const PACKAGE_REGEX = /[^]*^Package: (.*)[^]*$/m

// Depends is a comma-separated list of dependencies that
// a) may contain pipe-separated alternatives (which we put in a list)
// b) may contain version requirements (which we ignore)
const DEPENDENCY_REGEX = /[^]*^Depends: (.*)[^]*$/m
const DEPENDENCY_SEPARATOR = ', '
const ALTERNATIVE_SEPARATOR = ' | '
const VERSION_REGEX = /(.*) \(.*\)/

// I admit defeat; I can't figure out a way to do this in one regex
// Description can span multiple lines
// if it does, subsequent lines start with a space
// empty lines are made with a space followed by a dot
const DESCRIPTION_LAST_FIELD_REGEX = /[^]*^Description: ([^]*)$/m
const DESCRIPTION_OTHER_FIELD_REGEX = /[^]*^Description: ([^]*)^[^\s][^]*/m

// all of these matching regular expressions include a parenthesized section
// which is the part we want to retain
// --> we replace the whole match with only the part in parentheses
const REPLACEMENT = '$1'

// FIRST PASS: grab the package names and descriptions
const parsePackageNamesAndDescriptions = (entireFile) => {

  // the file is separated into paragraphs that are separated by two newlines
  // in unix newline is \n, in windows it's \r\n
  // let's try to accommodate both
  const PACKAGE_SEPARATOR = (entireFile.match(/\r/g) != null) ? '\r\n\r\n' : '\n\n'

  var packages = {}

  // "package" is a reserved word :(
  entireFile.split(PACKAGE_SEPARATOR).map((pkg) => {
    // init each package we discover
    if (pkg.match(PACKAGE_REGEX) != null) {
      const name = pkg.replace(PACKAGE_REGEX, REPLACEMENT)
      packages[name] = {
        name: name,
        text: pkg,
        dependencies: [],
        reverseDependencies: []
      }
      // if the package also contains a description, add that
      if (pkg.match(DESCRIPTION_OTHER_FIELD_REGEX) != null) {
        const description = pkg.replace(DESCRIPTION_OTHER_FIELD_REGEX, REPLACEMENT)
        packages[name].description = description
      } else if (pkg.match(DESCRIPTION_LAST_FIELD_REGEX) != null) {
        const description = pkg.replace(DESCRIPTION_LAST_FIELD_REGEX, REPLACEMENT)
        packages[name].description = description
      }
    }
  })
  return packages
}

// SECOND PASS: parse the other required fields
const parseDependencies = (packages) => {
  Object.keys(packages).map((pkgName) => {
    var pkg = packages[pkgName]
    if (pkg.text.match(DEPENDENCY_REGEX) != null) {
      // text version of dependencies
      const dependencies = pkg.text.replace(DEPENDENCY_REGEX, REPLACEMENT)

      // split comma-separated list into dependencies array
      const dependencyList = dependencies.split(DEPENDENCY_SEPARATOR)

      // split pipe-separated list into alternatives array
      dependencyList.map((dep) => {
        var alternatives = []
        dep.split(ALTERNATIVE_SEPARATOR).map((alt) => {
          // strip version requirement and whitespace
          if (alt.match(VERSION_REGEX) != null) {
            alt = alt.replace(VERSION_REGEX, REPLACEMENT)
          }
          alt = alt.trim()

          // add to dependencies both ways (if possible)
          const installed = (packages[alt] != undefined)
          alternatives.push({
            name: alt,
            installed: installed
          })
          if (installed) {
            packages[alt].reverseDependencies.push(pkg.name)
          }
        })
        pkg.dependencies.push(alternatives)
      })
    }

    // finally remove the text as we have no use for it anymore
    delete pkg.text
  })
  return packages
}

// first we parse the package names and descriptions, that creates an Object where
// 1) field names correspond to package names
// 2) the payload contains a "text" field which contains the entire paragraph
// this is then passed to parseDependencies to create the dependency graph
const parsePackages = (entireFile) => {
  return parseDependencies(parsePackageNamesAndDescriptions(entireFile))
}

module.exports = parsePackages
