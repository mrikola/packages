// NB: old school function declarations are used everywhere in this file
// Vue forbids arrow notation because "this" binding won't work under it

const infoComponent = {
  props: ['pkg'],
  methods: {
    // as mentioned in index.html, this creates an event that is then
    // sent up to the main Vue instance through the @details attribute
    showDetails: function (name) {
      this.$emit('details', name)
    }
  }
}


// the main Vue object is not quite that simple :)

const app = new Vue({
  el: '#app',
  data: {
    packages: undefined,
    currentPackage: undefined,
    sortedKeys: []
  },
  components: {
    'info': infoComponent
  },
  methods: {
    showDetails: function(name) {
      this.currentPackage = this.packages[name]
    },

    // take the file and send its contents to handleFile() in base64 format
    upload: function(e) {
      const that = this
      const fr = new FileReader()
      fr.addEventListener('load', function() {
        that.handleFile('upload', fr.result)
      }, false)
      fr.readAsDataURL(e.target.files[0])
    },

    // ajax the file back to the backend
    handleFile: function(param, value) {
      const that = this
      nanoajax.ajax({
        url: '#',
        method: 'POST',
        responseType: 'json',
        body: param + '=' + value
      }, function (code, response) {
        that.packages = response
        that.sortedKeys = Object.keys(response).sort() // for convenience
        that.currentPackage = undefined // clear the info component
      })
    }
  }
})
