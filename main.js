const app = require('express')()
const fs = require('fs')
const http = require('http')
const url = require('url')
const bodyParser = require('body-parser')
const dataurl = require('dataurl')

const parsePackages = require('./packageParser.js')

const SERVER_PORT = process.env.PORT || 8080;
const DEFAULTFILENAME = __dirname+'/data/status'

const getTestData = (fileID) => {
  var fileName = ''
  switch(fileID) {
    // if there were more test cases, I'd put them here
    case '1':
    default:
      fileName = DEFAULTFILENAME
  }
  var file = fs.readFileSync(fileName, {encoding: 'utf-8'})
  return parsePackages(file)
}


// FIXME: this whole thing is kinda ugly, should be nicely configured in a real app
const server = app.use((request, response, next) => {
  next()
}).use(bodyParser.urlencoded(
  { extended: true, limit: '10mb' }
)).get('*', (request, response, next) => {
  var requestUrl = url.parse(request.url)

  //  everything that's servable resides in /ui
  var fsPath = __dirname + '/ui' + requestUrl.pathname
  try {
    if (fs.statSync(fsPath).isDirectory()) fsPath += 'index.html';
  } catch(notfound) {
    response.writeHead(404)
    response.end()
    return
  }

  var fileStream = fs.createReadStream(fsPath)
  fileStream.on('error',function(e) {
    console.log(e)
  })
  response.writeHead(200)
  fileStream.pipe(response)
  next()
}).post('*', (request, response, next) => {
  // check for different types of POST, act accordingly
  // again, a real app would do this better, this is quick'n'dirty
  var fileContent = request.body.upload
  var testFileID = request.body.test

  if (testFileID != undefined) {
    response.status(200).send(JSON.stringify(getTestData(testFileID)))
  } else if (fileContent != undefined) {
    // plus signs get automagically converted to spaces, we don't want that
    var decodedContent = dataurl.parse(fileContent.replace(/ /g, '+')).data.toString()
    response.status(200).send(JSON.stringify(parsePackages(decodedContent)))
  } else {
    response.status(500).send('')
  }
}).listen(SERVER_PORT, () => console.log(`Listening on ${ SERVER_PORT }`));
